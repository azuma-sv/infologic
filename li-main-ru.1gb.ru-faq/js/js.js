(function ($) {
  $(document).ready(function(){
    var $body = $('body');
    /**
     * Message boxes.
     */
    var $msgBoxes = $('#msg .msg_box');
    $msgBoxes.each(function(){
      // Click event to show poup box.
      $(this).find('.ms_r').click(function(e){
        e.preventDefault();
        var $popupWrapper = $(this).parent();
        $msgBoxes.removeClass('opened');
        if ($popupWrapper.find('.popup_ms').length) {
          $popupWrapper.closest('.msg_box').addClass('opened');
        }
      });
    })
    // MouseUp event to hide popup.
    $(document).mouseup(function(e){
      $msgBoxes.each(function(){
        $this = $(this);
        if (!$this.is(e.target) && $this.has(e.target).length === 0) {
          $this.removeClass('opened');
        }
      });
    });

    /**
     * Plus button to reveal field.
     */
    var $plus = $('#legal_cont li.plus, #individual_cont li.plus, #account_cont li.plus');

    $fields = $plus.parent().next().hide()
    $plus.click(function(e){
      e.preventDefault();
      $(this).hide();
      $(this).parent().next().toggle();
    });

    /**
     * Email validation.
     */
    var $fields = $('#account_cont input[name="mainusername"], .popup-default.balance input[name="balance"]');
    $fields.each(function(){
      var $field = $(this);
      $field.wrap('<span class="validation-wrapper validation-required"></span>')
      $message = $('<span class="validation-message">Обязательное поле</span>').insertAfter($field);
      $field.change(function(){
        if ($(this).val() != '') {
          $message.css('opacity', '0');
        }
        else {
          $message.css('opacity', '1');
        }
      });
    });

    /**
     * Yandex map alter.
     */
    var waitYmap = function() {
      if ($body.hasClass('ymap-alter')) {
        var $ymap_placemark = $('ymaps.ymaps-2-1-31-svg-icon');
        if ($ymap_placemark.length != 1) {
          setTimeout(waitYmap, 2000);
        }
        else {
          $ymap_placemark.append('<div class="ymap-place"><div class="ymap-text">Россия, Москва, улица Сущёвский Вал, 75с1</div><div class="arrow"></div></div>');
        }
      }
    };
    waitYmap();

    /**
     * Balance popup JS.
     */
    var $popupBalance = $('.popup-default.balance'),
        $triggerOne = $('span#popup_pop_h'),
        $triggerTwo = $('span#popup_pr_h'),
        $inputs = $popupBalance.find('.item-balance-type input'),
        $input = $popupBalance.find('.item-balance input'),
        $range = $('.balance-form-wrapper .item-balance-range .balance-range');
    // Popup opening.
    if ($triggerOne.length == 1) {
      $triggerOne.css('cursor', 'pointer');
      $triggerOne.click(function(e){
        e.preventDefault();
        $popupBalance.show();
        $popupBalance.removeClass('second');
      });
    }
    // Second trigger.
    if ($triggerTwo.length == 1) {
      $triggerTwo.css('cursor', 'pointer');
      $triggerTwo.click(function(e){
        e.preventDefault();
        $popupBalance.show();
        if (!$popupBalance.hasClass('second')) {
          $popupBalance.addClass('second');
        }
      });
    }
    // Price update.
    $inputs.change(function(){
      $(this).closest('form').find('.item-balance input').val($(this).attr('setprice')).change();
      $range.val($(this).attr('setprice'));
    });
    // Price update.
    $range.on('input', function(){
      if ($(this).val() < 0) {
        $(this).val(0);
      }
      if ($(this).val() > 4000) {
        $(this).val(4000);
      }
      $input.val($(this).val()).change();
      balanceRadioCheck($(this).val());
    });
    // Balance type update.
    $input.on('input', function() {
      var $this = $(this),
          $value = parseInt($this.val());

      // Validate value.
      $value = isNaN($value) ? 0 : $value;
      $value = $value > 9999999999 ? 9999999999 : $value
      $value = $value < 1 ? '' : $value;
      $this.val($value);

      // Update balance type.
      balanceRadioCheck($value);
      $range.val($value > 4000 ? 4000 : $value);
    });

    // Update balance type.
    balanceRadioCheck = function(value) {
      if (value <= 1000) {
        $inputs.first().prop("checked", true);
      }
      else if (value <= 3000) {
        $inputs.eq(1).prop("checked", true);
      }
      else {
        $inputs.last().prop("checked", true);
      }
    }

    /**
     * Default popup JS
     */
    var $defaultPopup = $('.popup-default');
    $defaultPopup.each(function(){
      var $this = $(this);
      $this.find('.p-close').click(function(e){
        e.preventDefault();
        $(this).closest('.popup-default').hide();
      });
    });

    /**
     * Search cargo map.
     */
    // Function to show / hide YMap.
    var toggleMap = function(event) {
      var state = event.data.state;
      event.preventDefault();

      if (state == 'undefined') {
        // Just toggle.
        $('#favorit_cont, #ymap-gr').toggle();
        $body.toggleClass('ymap-opened');
      }
      else {
        // If state has been passed.
        if (state) {
          // Enable map.
          if(!$body.hasClass('ymap-opened')){
            $body.addClass('ymap-opened');
          }
          $('#favorit_cont').hide();
          $('#ymap-gr').show();
        }
        else {
          // Disable map.
          if($body.hasClass('ymap-opened')){
            $body.removeClass('ymap-opened');
          }
          $('#favorit_cont').show();
          $('#ymap-gr').hide();
        }
      }

      // Show active link.
      var $listButton = $searchButtons.find('a').first(),
          $mapButton = $searchButtons.find('a').last();
      if ($body.hasClass('ymap-opened')) {
        $listButton.removeClass('active');
        if(!$mapButton.hasClass('active')){
          $mapButton.addClass('active');
        }
      }
      else {
        $mapButton.removeClass('active');
        if(!$listButton.hasClass('active')){
          $listButton.addClass('active');
        }
      }
    }

    // Init buttons.
    var $searchButtons = $('#load_right');
    if ($searchButtons.length == 1) {
      // Init variables.
      var $listButton = $searchButtons.find('a').first(),
          $mapButton = $searchButtons.find('a').last();

      // Click triggers.
      $mapButton.click({state: true}, toggleMap);
      $listButton.click({state: false}, toggleMap);

      // Add wrapper for map.
      var $YMap = $('<div id="ymap-gr" class="ymap-gr"></div>').appendTo($('body'));

      // Init map when ready.
      ymaps.ready(function(){
        var YMap = new ymaps.Map("ymap-gr", {
          center: [55.76, 37.64],
          zoom: 10
        });

        var $baloons = $('#favorit_cont baloon');
        $baloons.each(function(){
          var $this = $(this),
              placeX = $this.attr('place-x'),
              placeY = $this.attr('place-y'),
              content = '<ul class="ymap-baloon-text">' + $this.parent().parent().html() + '</ul>';
          var point = new ymaps.Placemark([placeX, placeY], {
            balloonContent: content,
          }, {
            iconLayout: 'default#image',
            iconImageHref: 'images/baloone.png',
            iconImageSize: [44, 64],
            iconImageOffset: [-22, -64],
          });
          YMap.geoObjects.add(point);
        });
      });
    }

    /**
     * Calendar initialization.
     */
    var $calendarInput = $('#calendar-dateperiod');
    if ($calendarInput.length == 1) {
      // Set first/last classes for datepicker days.
      var calendarFirstLast = function($container){
        var $selected = $container.find('.datepickerSelected');
        $selected.first().addClass('first');
        $selected.last().addClass('last');
      }

      // Init datepicker.
      $calendarInput.DatePicker({
        format:'d.m.Y',
        date: $calendarInput.val().split(' — '),
        mode: 'range',
        starts: 1,
        position: 'right',
        locale: {
          days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Субота", "Воскресенье"],
          daysShort: ["Вскр", "Пон", "Втр", "Ср", "Чт", "Пт", "Сбт", "Вскр"],
          daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"],
          months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сетябрь", "Октябрь", "Ноябрь", "Декабрь"],
          monthsShort: ["Янв", "Фев", "Мрт", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Нбр", "Дек"],
          weekMin: '№Н'
        },
        onChange: function(formated, dates) {
          // Set value to input.
          $calendarInput.val(formated[0] + ' — ' + formated[1]);
          // Close calendar if we need.
          if (formated[0] != formated[1]) {
            // $calendarInput.DatePickerHide();
          }
          // Set first/last classes for datepicker days.
          calendarFirstLast($(this));
        },
        onShow: function(container) {
          var $container = $(container);
          // Set correct position.
          var $pos = $(this).position();
          $container.css({left: $pos.left + $(this).outerWidth() + 10, top: $pos.top - 188});
          // Set first/last classes for datepicker days.
          calendarFirstLast($container);
          // Add few html elements.
          $container.append('<div class="arrow"><span></span></div>');
          $container.append('<div class="button-ready">Готово</div>');
          $container.find('.button-ready').click(function(){
            $calendarInput.DatePickerHide();
          })
        },
        onRender: function(date) {
          var dateFormatted = ("0" + date.getDate()).slice(-2) + '.' + ("0"+(date.getMonth()+1)).slice(-2) + '.' + date.getFullYear();
              inputDates = $calendarInput.val().split(' — '), classes = {className: ''};
          // Set first/last classes for datepicker days.
          if (inputDates[0] == dateFormatted) {
            classes.className = classes.className + ' first';
          }
          if (inputDates[1] == dateFormatted) {
            classes.className = classes.className + ' last';
          }
        return classes;
        }
      });
    }

    /**
     * Small popup "Add from history".
     */
    var $addFromHist = $('.popup-item-list');
    $addFromHist.click(function () {
      $(this).toggleClass('opened');
    })
  });
})(jQuery);
